import base64
import requests
import json
import urllib3
import xlrd
import argparse
import logging


def get_logger(name):
    logger = logging.getLogger(name)
    formatter = logging.Formatter("%(asctime)s %(levelname)7s %(funcName)40s() - %(message)s")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger

logger = get_logger("TestRailUtilities")

class Api():
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.base_url = "https://redlock.testrail.io/index.php?/api/v2/"
        self.headers = {
            "Content-Type": "application/json"
        }
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def _getRequest(self, resource_url):
        url = self.base_url + resource_url
        logger.info("GET [{}]".format(url))
        response = requests.get(url,auth=(self.username,self.password),headers=self.headers,verify=False)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        else:
            raise Exception("GET Request Failed url:{} Status Code:{}".format(url, response.status_code))

    def _postRequest(self, resource_url, data):
        url = self.base_url + resource_url
        logger.info("POST [{}]".format(url))
        response = requests.post(url,data=json.dumps(data),auth=(self.username,self.password),headers=self.headers,verify=False)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        else:
            raise Exception("POST Request Failed url:{} Status Code:{} Response : {}".format(url, response.status_code, response.content))

    def _putRequest(self, resource_url, data):
        url = self.base_url + resource_url
        logger.info("PUT [{}]".format(url))
        response = requests.put(url,data=data,auth=(self.username,self.password),headers=self.headers,verify=False)
        if response.status_code == requests.codes.ok:
            return json.loads(response.content)
        else:
            raise Exception("PUT Request Failed url:{} Status Code:{} Response : {}".format(url, response.status_code, response.content))


class TestRail(Api):
    
    def __init__(self, username, password, project_id):
        self.project_id = project_id
        self.template_id = 4
        Api.__init__(self, username, password)
        self._fields = {
            "section_id" : {
                "system_name" : "section_id",
                "label" : "Section ID",
                "required" : True
            },
            "id" : {
                "system_name" : "id",
                "label" : "ID",
                "required" : True
            },
            "type_id" : {
                "system_name" : "type_id",
                "label" : "Test Case Type",
                "required" : True
            },
            "title" : {
                "system_name" : "title",
                "label" : "Title",
                "required" : True
            },
            "priority_id" : {
                "system_name" : "priority_id",
                "label" : "Priority",
                "required" : True
            },
            "refs" : {
                "system_name" : "refs",
                "label" : "Jira References",
                "required" : False
            },
            "custom_automation_type" : {
                "system_name" : "custom_automation_type",
                "label" : "Automated Status",
                "required" : True
            },
            "custom_preconds" : {
                "system_name" : "custom_preconds",
                "label" : "Pre conditions",
                "required" : False
            },
            "custom_steps" : {
                "system_name" : "custom_steps",
                "label" : "Steps",
                "required" : True
            },
            "custom_expected" : {
                "system_name" : "custom_expected",
                "label" : "Expected",
                "required" : True
            },
            "custom_tags" : {
                "system_name" : "custom_tags",
                "label" : "Test Cycle",
                "required" : True
            }
        }
        self._updateTestCaseFields()
        self._updateCaseTypes()
        self._updatePriorities()
    
    def _updateCaseTypes(self):
        try:
            logger.info("updateCaseTypes -> STARTED")
            resource_url = "get_case_types"
            case_types = self._getRequest(resource_url)
            name = "type_id"
            supported_values = dict()
            for ctype in case_types:
                supported_values[int(ctype["id"])] = ctype["name"]
            self._fields[name]["values"] = supported_values
            logger.info("updateCaseTypes -> COMPLETED")
        except Exception as e:
            raise Exception("updateCaseTypes : {}".format(str(e)))
  
    def _updatePriorities(self):
        try:
            logger.info("updatePriorities -> STARTED")
            resource_url = "get_priorities"
            priorities_types = self._getRequest(resource_url)
            name = "priority_id"
            supported_values = dict()
            for ptype in priorities_types:
                supported_values[int(ptype["id"])] = ptype["name"]
            self._fields[name]["values"] =  supported_values
            logger.info("updatePriorities -> COMPLETED")
        except Exception as e:
            raise Exception("updatePriorities : {}".format(str(e)))
    
    def _updateTestCaseFields(self):
        logger.info("_updateTestCaseFields -> STARTED")
        try:
            resource_url = "get_case_fields"
            custom_fields = self._getRequest(resource_url)
            for fields in custom_fields:
                if self.template_id in fields["template_ids"] or len(fields["template_ids"])==0:
                    if fields["is_active"]:
                        name = fields["system_name"]
                        if "items" in fields["configs"][0]["options"].keys():
                            supported_values = dict()
                            value_list = fields["configs"][0]["options"]["items"]
                            value_pair = value_list.split("\n")
                            for pair in value_pair:
                                key_val = pair.split(",")
                                supported_values[int(key_val[0].strip())] = key_val[1].strip()
                            self._fields[name]["values"] = supported_values
            logger.info("_updateTestCaseFields -> COMPLETED")
        except Exception as e:
            raise Exception("_updateTestCaseFields : {}".format(str(e)))

    def isRequired(self, field_name):
        return self._fields[field_name]["required"]

    def getSupportedValues(self, field_name):
        return self._fields[field_name]["values"]

    def getLabel(self, field_name):
        return self._fields[field_name]["label"]

    def getTagNameById(self, ids):
        logger.info("getTagNameById -> STARTED")
        id_list = ids.split(",")
        tag_name_list = list()
        for id in id_list:
            tag_name_list.append(self.getSupportedValues("custom_tags")[id])
        logger.info("getTagNameById -> COMPLETED")
        return ",".join(tag_name_list)

    def getRequiredFieldNames():
        return self._fields.keys()

    def getComponentsById(self, ids):
        logger.info("getComponentsById -> STARTED")
        id_list = ids.split(",")
        comp_list = list()
        for id in id_list:
            comp_list.append(self.getSupportedValues("components")[id])
        logger.info("getComponentsById -> COMPLETED")
        return ",".join(comp_list)

    def getTestCasesBySection(self, section_id):
        resource_url = "get_cases/" + str(self.project_id) + "&section_id=" + str(section_id)
        return self._getRequest(resource_url)

    def addTestCase(self, payload):
        section_id = payload["section_id"]
        resource_url = "add_case/" + str(section_id)
        self._postRequest(resource_url, payload)
        logger.info("Successfully Added Test Case")

    def updateTestCase(self, payload):
        case_id = payload["id"]
        resource_url = "update_case/" + str(case_id)
        self._postRequest(resource_url, payload)
        logger.info("Successfully Updated Test Case for id " + str(case_id))

    def getSections(self):
        resource_url = "get_sections/" + str(self.project_id)
        return self._getRequest(resource_url)

    def sanitizeRequest(self, request):
        request["template_id"] = 4
        request["suite_id"] = 18
        if "created_on" in request.keys():
            del request["created_on"]
        if "updated_on" in request.keys():
            del request["updated_on"]
        return request

    def transformFieldsForUpload(self, field_name, value):
        try:
            logger.info("transformFieldsForUpload -> {} : {}".format(field_name, value))
            if "values" in self._fields[field_name].keys():
                supported_vals = self.getSupportedValues(field_name)
                if field_name == "custom_tags":
                    all_tags = value.split(",")
                    result = list()
                    for tag in all_tags:
                        for key in supported_vals.keys():
                            if supported_vals[key].lower() == tag.lower():
                                result.append(key)
                                break
                    logger.info("Field transformed -> {} : {}".format(field_name, result))
                    return result
                else:
                    for key in supported_vals.keys():
                        if supported_vals[key].lower() == value.lower():
                            logger.info("Field transformed -> {} : {}".format(field_name, key))
                            return key
            return value
        except Exception as e:
            raise Exception("transformFieldsForUpload : {}".format(str(e)))
  
    def transformFieldsForFetch(self, field_name, value):
        try:
            logger.info("transformFieldsForFetch -> {} : {}".format(field_name, value))
            if "values" in self._fields[field_name].keys():
                if field_name == "custom_tags":
                    return self.getTagNameById(value)
                else:
                    return self._fields[field_name]["values"][value]
            else:
                return value
        except Exception as e:
            raise Exception("transformFieldsForFetch : {}".format(str(e)))

    def upload(self, xl_path):
        xl_workbook = xlrd.open_workbook(xl_path)
        xl_sheet = xl_workbook.sheet_by_index(0)
        logger.info("Reading Sheet : {}".format(xl_sheet.name))
        rows = xl_sheet.nrows
        cols = xl_sheet.ncols
        test_cases = list()
        for row in range(1,rows):
            try:
                logger.info("Uploading row : {}".format(row))
                test_case = dict()
                test_case = self.sanitizeRequest(test_case)
                for col in range(cols):
                    col_name = xl_sheet.cell(0,col).value
                    col_value = xl_sheet.cell(row,col).value
                    if col_value:
                        col_value = self.transformFieldsForUpload(col_name, col_value)
                    test_case[col_name] =  int(col_value) if isinstance(col_value, float) else col_value
                test_cases.append(test_case)
                if "id" in test_case.keys() and test_case["id"]:
                    self.updateTestCase(test_case)
                else:
                    self.addTestCase(test_case)
            except Exception as e:
                logger.error("Upload Exception : {}".format(str(e)))
            
def uploadTestCases(username, password, path):
    testrail = TestRail(username,password, 2)
    testrail.upload(path)        

def fetchTestCases(path):
    test_cases = SpreadSheetApi.readSheet(path)
    fields = SpreadSheetApi.getTemplate()
    test_file = open("test_case_file.json","w+")
    test_file.write(json.dumps(test_cases))
    test_file.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Upload Test Cases on TestRail from Qualys")
    parser.add_argument("--action", help="server to be cleaned")
    parser.add_argument("--path", help="server to be cleaned")
    parser.add_argument("--username", default="akasingh@paloaltonetworks.com", help="Username for Test Rail")
    parser.add_argument("--password", default="Abcd_1234", help="Password for Test Rail")

    args = parser.parse_args()
    if args.action.lower() == "upload":
        uploadTestCases(args.username, args.password, args.path)